from django.urls import path
from gpon import views

urlpatterns = [
    path('', views.index, name='index'),
]
