from django.shortcuts import render
from gpon.models import Gpons

# Create your views here.
def index(request):
    keys = Gpons.objects.using('gpon')
    return render(request, 'pages/index.html', { 'keys': keys })
