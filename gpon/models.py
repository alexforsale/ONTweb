from django.db import models

class Gpons(models.Model):
    gpon_port = models.CharField(max_length=255, blank=True, null=True)
    ont_id = models.IntegerField(blank=True, null=True)
    run_state = models.CharField(max_length=255, blank=True, null=True)
    last_uptime = models.CharField(max_length=255, blank=True, null=True)
    last_downtime = models.CharField(max_length=255, blank=True, null=True)
    last_downcause = models.CharField(max_length=255, blank=True, null=True)
    sn = models.CharField(primary_key=True, max_length=255)
    type = models.CharField(max_length=255, blank=True, null=True)
    distance = models.CharField(max_length=255, blank=True, null=True)
    rx_tx_power = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'gpons'
